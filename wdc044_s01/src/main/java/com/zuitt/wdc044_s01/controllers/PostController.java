package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

        //get all posts from all users
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //create a new post
    @RequestMapping(value="/users/{userid}/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@PathVariable Long userid, @RequestBody Post post){
        System.out.println("userid = " + userid);
        postService.createPost(userid, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/users/{userid}/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@PathVariable Long userid){
        return new ResponseEntity<>(postService.getMyPosts(userid), HttpStatus.OK);
    }

}
