package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl  implements  PostService{
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;

    public void createPost(Long userid, Post post) {
        User author = userRepository.findById(userid).get();
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }


    public void updatePost(Long id, Post post){
        Post postForUpdating = postRepository.findById(id).get();
        postForUpdating.setTitle(post
.getTitle());
        postForUpdating.setContent(post.getContent());
        postRepository.save(postForUpdating);
}

public void deletePost(Long id){
        postRepository.deleteById(id);
}

public Iterable<Post> getMyPosts(Long userid){
        User author = userRepository.findById(userid).get();
        return author.getPosts();
}
public Iterable<Post> getPosts(){
        return postRepository.findAll();
}
}


